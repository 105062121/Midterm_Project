firebase.auth().onAuthStateChanged(function (user) {
  var menu = document.getElementById('user-acc');
  // Check user login
  if (user) {
      user_email = user.email;
      menu.innerHTML = "<span class='text-white'>" + user.email + "</span><br><button class='btn btn-lg btn-secondary' id='merch-btn'>Merch List</button><br><button class='btn btn-lg btn-secondary' id='logout-btn'>Logout</button>";
      document.getElementById("logout-btn").addEventListener("click", function () {
          firebase.auth().signOut().then(function () {
              alert("success");
              window.location = "index.html";
          }).catch(function (error) {
              alert("failed");
          });
      });
      document.getElementById("merch-btn").addEventListener("click", function () {
        window.location = "merch.html";
      });

      firebase.database().ref(firebase.auth().currentUser.uid).once('value', function(snapshot){
        snapshot.forEach(function(childSnapshot){
          if(childSnapshot.child('merch').val()=="stardust"){
            document.getElementById('record').innerHTML += "<a class='btn btn-lg btn-block btn-basic' style='cursor: context-menu'><img src='images/sword.ico' height='50' width='50'>&nbsp; Stardust Sword &nbsp;"+ childSnapshot.child('date').val() + " " + childSnapshot.child('time').val() + "</a><p></p>";
          }
          else if(childSnapshot.child('merch').val()=="spirit"){
            document.getElementById('record').innerHTML += "<a class='btn btn-lg btn-block btn-basic' style='cursor: context-menu'><img src='images/spear.ico' height='50' width='50'>&nbsp; Spear of the Spirits &nbsp;"+ childSnapshot.child('date').val() + " " + childSnapshot.child('time').val() + "</a><p></p>";
          }
          else if(childSnapshot.child('merch').val()=="light"){
            document.getElementById('record').innerHTML += "<a class='btn btn-lg btn-block btn-basic' style='cursor: context-menu'><img src='images/staff.ico' height='50' width='50'>&nbsp; Arclight Staff &nbsp;"+ childSnapshot.child('date').val() + " " + childSnapshot.child('time').val() + "</a><p></p>";
          }
          else if(childSnapshot.child('merch').val()=="gringham"){
            document.getElementById('record').innerHTML += "<a class='btn btn-lg btn-block btn-basic' style='cursor: context-menu'><img src='images/gringham.ico' height='50' width='50'>&nbsp; Gringham Whip &nbsp;"+ childSnapshot.child('date').val() + " " + childSnapshot.child('time').val() + "</a><p></p>";
          }
          else if(childSnapshot.child('merch').val()=="fear"){
            document.getElementById('record').innerHTML += "<a class='btn btn-lg btn-block btn-basic' style='cursor: context-menu'><img src='images/axe.ico' height='50' width='50'>&nbsp; Axe of Fear &nbsp;"+ childSnapshot.child('date').val() + " " + childSnapshot.child('time').val() + "</a><p></p>";
          }
          else if(childSnapshot.child('merch').val()=="metal"){
            document.getElementById('record').innerHTML += "<a class='btn btn-lg btn-block btn-basic' style='cursor: context-menu'><img src='images/shield.ico' height='50' width='50'>&nbsp; Metal King Shield &nbsp;"+ childSnapshot.child('date').val() + " " + childSnapshot.child('time').val() + "</a><p></p>";
          }
        });
    });
  };
});