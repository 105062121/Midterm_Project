function initApp() {
  var txtName = document.getElementById('inputName');
  var txtEmail = document.getElementById('inputEmail');
  var txtPhone = document.getElementById('inputPhone');
  var txtAddress = document.getElementById('inputAddress');
  var btnOrder = document.getElementById('btnOrder');

  var productselect = window.location.search.substring(1);

  if(productselect=="stardust"){
    document.getElementById('item-ico').src = "images/sword.ico";
    document.getElementById('item-name').innerHTML = "Stardust Sword $3500";
  }
  else if(productselect=="spirit"){
    document.getElementById('item-ico').src = "images/spear.ico";
    document.getElementById('item-name').innerHTML = "Spear of the Spirit $3500";
  }
  else if(productselect=="light"){
    document.getElementById('item-ico').src = "images/staff.ico";
    document.getElementById('item-name').innerHTML = "Arclight Staff $3500";
  }
  else if(productselect=="gringham"){
    document.getElementById('item-ico').src = "images/gringham.ico";
    document.getElementById('item-name').innerHTML = "Gringham Whip $3500";
  }
  else if(productselect=="fear"){
    document.getElementById('item-ico').src = "images/axe.ico";
    document.getElementById('item-name').innerHTML = "Axe of Fear $3500";
  }
  else if(productselect=="metal"){
    document.getElementById('item-ico').src = "images/shield.ico";
    document.getElementById('item-name').innerHTML = "Metal King Shield $3500";
  }

  btnOrder.addEventListener('click', function () {
      var name = txtName.value;
      var email = txtEmail.value;
      var phone = txtPhone.value;
      var address = txtAddress.value;
      if(name!="" && email!="" && phone!="" && address!=""){
        var date = new Date();
        var thisYear = date.getFullYear();
        var thisMonth = date.getMonth()+1;
        var thisDay = date.getDate();
        var today = thisYear + "-" + thisMonth + "-" + thisDay;
        var thisHour = date.getHours();
        var thisMinute = date.getMinutes();
        var now = thisHour + ":" + thisMinute;
        firebase.database().ref(firebase.auth().currentUser.uid).push({
          merch: window.location.search.substring(1),
          date: today,
          time: now
        });
        console.log("success");
        setTimeout(function(){ window.location="thanks.html"; }, 2000);
      }
      else{
        create_alert("error", "There are some unexpected or invalid inputs");
      }
  });
}

function create_alert(type, message) {
  var alertarea = document.getElementById('custom-alert');
  if (type == "success") {
      str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
      alertarea.innerHTML = str_html;
  } else if (type == "error") {
      str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
      alertarea.innerHTML = str_html;
  }
}

window.onload = function () {
  initApp();
};