function init() {
  var user_email = '';
  firebase.auth().onAuthStateChanged(function (user) {
      var menu = document.getElementById('user-acc');
      // Check user login
      if (user) {
          user_email = user.email;
          menu.innerHTML = "<span class='text-white'>" + user.email + "</span><br><button class='btn btn-lg btn-secondary' id='history-btn'>Purchase History</button><br><button class='btn btn-lg btn-secondary' id='logout-btn'>Logout</button>";
          document.getElementById("logout-btn").addEventListener("click", function () {
              firebase.auth().signOut().then(function () {
                  alert("success");
                  window.location = "index.html";
              }).catch(function (error) {
                  alert("failed");
              });
          });
          document.getElementById("history-btn").addEventListener("click", function () {
            window.location = "record.html";
          });
      };
  });
};

window.onload = function () {
  init();
}