# Software Studio 2018 Spring Midterm Project

## Topic
* Shopping Web Page
* Key functions
    1. Product Page
    2. Shopping pipeline: Cash on Delivery
    3. User Purchase History (Database read/write)
* Other functions
    1. You can know when you purchase the product in Purchase History

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|N|
|Security Report|5%|N|
|Other functions|1~10%|Y|

## Website Detail Description
* Welcome → Sign in/Sign up → See merch → Merch info → Order merch → Back to "See merch"
* See merch → Purchase History
* All templates are from https://getbootstrap.com/docs/4.1/examples/